Italian Keyboard Layout for Windows with tilde (~) and backtrack (`)

[Italiano sotto]

Quick install:

- Download: https://bitbucket.org/lorenzosu/win-it_tilde-keyboard-layout/downloads/it_tilde.zip

- Unzipi somewhere

- Run setup.exe in the it_tilde directory (you need Administrator priviledges)


ENGLISH
=======
The default Italian keyboard layouts on Windows lack a way of inputting ~ and ` 

This keyboard layout fixes this by making them available as follows:

~ -> AltGr + ì

` -> AltGr + '

Reason for this mapping is that it is the same as the one found for Italian layouts on all Linux distributions I used so far.

The layout was created using the Microsoft Keyboard Layout Creator: http://www.microsoft.com/en-us/download/details.aspx?id=22339. It should work from Windows XP and above. Tested on Windows 7 64bit

The layout creator is unclear on licensing terms but this shoud obviously considered 'as is'

ITALIANO
=========
I layout di tastiera italiani per Windows non hanno mai avuto ~ e `

Questo layout li mette a disposizione con le seguenti combinazioni.

~ -> AltGr + ì

` -> AltGr + '

Il motivo di questa mappatura è che è la stessa che ho trovato sui layout italini in tutte le ditribuzioni Linux usate.

Il layout è stato creato con il Microsoft Keyboard Layout Creator: http://www.microsoft.com/en-us/download/details.aspx?id=22339. Dovrebbe funzionare da Windows XP in poi. Testato su Windows 7 64bit.

La licenza del software creator non è chiara sulla re-distribuzione ma ovviamente questo è da considerare "così com'è"
